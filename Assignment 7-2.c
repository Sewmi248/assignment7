//print frequency of a character in a string
#include <stdio.h>
#include<string.h>
int main()
{
    char ch;
    char sen[100];

   int i, count=0;
    printf("Enter string: ");
    gets(sen);

    printf("Enter character: ");
    scanf("%c", &ch);

    int length=strlen(sen);

    for (i = 0; i<length; ++i) {
        if (ch == sen[i])
            ++count;
    }
    printf("Frequency of %c : %d", ch, count);
    return 0;
}
