//addition and multiplication of two matrices
#include <stdio.h>
#define rows 2
#define cols 2

int main()
{
    int array1[rows][cols]={{2, 3}, {4, 5}};
   int array2[rows][cols]={{4, 8}, {6, 7}};
   int mul[rows][cols];
    int i, j, k;
    printf("First matrix\n");
    for (i = 0; i<rows; i++) {
        for (j = 0; j<cols; j++) {
            printf("%d ", array1[i][j]);

    }
     printf("\n");
    }
    printf("\n");

     printf("second matrix\n");
     for (i = 0; i<rows; i++) {
        for (j = 0; j<cols; j++) {
            printf("%d ", array2[i][j]);

    }
     printf("\n");
    }
     printf("\n");

     printf("Addition of two matrices\n");
    for (i = 0; i<rows; i++) {
        for (j = 0; j<cols; j++) {
            printf("%d ", array1[i][j]+array2[i][j]);

    }
     printf("\n");
    }
     printf("\n");


   for(i=0;i<rows;i++)
    {
        for(j=0;j<cols;j++)
        {
            mul[i][j]=0;
            for(k=0;k<cols;k++)
            {
                mul[i][j]+=array1[i][k]*array2[k][j];
            }
        }
    }
     printf("Multiplication of two matrices\n");
    for (i = 0; i<rows; i++) {
        for (j = 0; j<cols; j++) {
            printf("%d ", mul[i][j]);

    }
     printf("\n");
    }

    return 0;
}
